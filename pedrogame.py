#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
 
class Unidad():
        name="Enemigo"
        nivel="1"
        hp="10"
        hpmax="10"
        atk="5"
        df="5"
        xp="10"
        basehp="2"
        baseatk="3"
        basedf="2"
        #con esto inicializamos un enemigo
        def stats(self,nombre,nivel,vida,ataque,defensa,exp):
                self.name=nombre
                self.nivel=nivel
                self.hp=vida*nivel
                self.hpmax=vida*nivel
                self.basehp=vida
                self.atk=ataque*nivel
                self.baseatk=ataque
                self.df=defensa*nivel
                self.basedf=defensa
                self.xp=exp*nivel
 
        def levelUp(self):
                self.nivel+=1
                self.hpmax+=self.basehp
                self.hp=self.hpmax
                self.atk+=self.baseatk
                self.df+=self.basedf
                print "Has subido al nivel ",self.nivel,"!"
 
        # Con esto muestras los stats del jugador
        def printEstado(self):
                return self.name + "\nHp: "+str(self.hp)+" Ataque: "+str(self.atk)+"  Defensa: "+str(self.df)
 
 
class Game:
        k=3
        kk=3
        damage=27
        xp=0
 
        # El constructor de la clase juego:
        def __init__(self):
                self.player = Unidad()
                self.player.stats("Guerrero de la Pena",3,50,5,1,0)
                self.enemy = Unidad()
                self.bcombate = False
                self.ordenes={"estado"  : self.printEstado,
                        "rename"        : self.renamePlayer,
	#                        "roll"          : self.roll,
                        "descansar"     : self.descansar,
                        "atacar"        : self.atacar,
                        "enemigo"       : self.printEnemyStats,
                        "batalla"       : self.newEnemy,
                        "help"          : self.helpMe,}
 
        # Aquí recibes la entrada del juego, y emites la salida que quieras:
        def input(self, string):
                if (self.ordenes.has_key(string)):
                        return self.ordenes[string]()
                return "No te entiendo, dame otra orden."
 
        # Con esto muestras los stats del jugador
        def printEstado(self):
                return self.player.printEstado()
        # Con esto muestras los stats del enemigo
        def printEnemyStats(self):
                return self.enemy.printEstado()
 
        #con esto cambiamos el nombre del personaje
        def renamePlayer(self):
                return "WIP"
 
        #numero aleatorio
#       def roll(self):
  #              print random.randint(1,5)
 
        #generar enemigo al azar
        def newEnemy(self):
                self.k=random.randint(1,4)
                self.kk=random.randint(self.player.nivel-3,self.player.nivel+4)
                if(self.k==1):
                        self.enemy.stats("Babosa",self.kk,10,3,0,10)
                if(self.k==2):
                        self.enemy.stats("Jabali",self.kk,27,6,0,15)
                if(self.k==3):
                        self.enemy.stats("Jabali Verrugoso",self.kk,57,5,2,20)
                if(self.k==4):
                        self.enemy.stats("Armadurillo",self.kk,10,3,15,30)
                self.bcombate=True
                return self.printEnemyStats()
 
        #recuperar vida
        def descansar(self):
                result = ""
                if(self.bcombate):
                        result += "Ahora no puedes descansar, estás en combate!"
                        result += self.enemyAttack()
                else:  
                        if(self.player.hp<self.player.hpmax):
                                self.player.hp=self.player.hpmax
                                result += "Has recuperado todos tus hp"
                        else:
                                result += "Ya tenias la salud completa!"
                return result
                               
        #atacar
        def atacar(self):
                result = ""
                if(self.bcombate):
                        k=random.randint(1,6)
                        if(k>1):
                                damage=self.player.atk-self.enemy.df+k
                                if(damage<0):
                                        damage=0
                                if(k>5):
                                        result += "BRUTAL CRITICAL STRIKE\n"
                                        damage+=self.player.atk+k
                                result += "Damage: "+str(damage)+"\n"
                                self.enemy.hp-=damage                  
                        else:
                                result += "Has fallado!\n"
                    #despues de atacar se acaba tu turno, y le toca al enemigo
                        if(self.enemy.hp>0):
                                result += "El enemigo sigue en pie con " + str(self.enemy.hp) + " hp\n"
                                result+=self.enemyAttack()
                        else:
                                result += "Has acabado con el enemigo!\n"
                                result += "Obtienes "+ str(self.enemy.xp*self.enemy.nivel) + " puntos de experiencia!\n"
                                self.xp+=self.enemy.xp
                                if(self.xp>self.player.nivel*100):
                                        self.xp=0
                                        self.player.levelUp
 
                                self.bcombate=False
                else:
                        result += "No hay ningun enemigo!"
                return result
 
 
 
        #el enemigo ataca!
        def enemyAttack(self):
                result = "El enemigo ataca!\n"
                k=random.randint(1,6)
                if(k>1):
                        damage=self.enemy.atk-self.player.df+k
                        if(damage<0):
                                damage=0
                        if(k>5):
                                result += "BRUTAL CRITICAL STRIKE\n"
                                damage+=self.enemy.atk+k
                        result += "Damage: "+str(damage)+"\n"
                        self.player.hp-=damage
                        if(self.player.hp>0):
                                result += "Has aguantado con: "+str(self.player.hp)+" hp"
                        else:
                      		 	result+=self.reset()
                     			result += "El enemigo ha acabado contigo!\n Estás acabado!\n \n ...\n \n New Game Started..."
                				
                else:
                        result += "Esquivaste el golpe!"
                return result;

        def reset(self):
 			result="..."
 			self.xp=0
 			self.player.stats("Guerrero de la Pena",3,30,5,1,0)
 			self.bcombate=False
 			return result;
 
        def helpMe(self):
                return "Si no sabes que hacer, estos son los comandos:\nestado:  comprueba tu estado\nrename:  dale un nombre digno a tu heroe\ndescansar:  date un respiro en la posada\nbatalla: busca pelea\natacar:  ataca al enemigo con la fuerza de una rosquilla!\nenemigo: comprueba el estado del enemigo\n" 
 
 
# Orden para salir
quitCommand = "exit"
 
# Una prueba del juego:
if __name__=="__main__":
 
        g=Game()
 
        x = raw_input()
        while (x != quitCommand):
                print g.input(x)
                x = raw_input()