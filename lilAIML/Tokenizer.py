#-*- coding: utf-8 -*-
tokens = ( 'WORD','URL', 'NUMBER','FIN',"SPACES",'UNKNOWN') 

t_WORD = ur'[\wñÑáÁéÉíÍóÓúÚ]+'
t_SPACES = r'\s+'
t_FIN = r'[\.\?!]'
t_UNKNOWN = r'.'

def t_URL(t):
	r'((https?:\/\/)|(www\.))([^\/:\s]*)(\.\w+)(:\d+)?(\/[^\s]*)?'
	if (not t.value.startswith("http")):
		t.value = "http://"+t.value
	return t

def t_NUMBER(t):
    r'\d+\.'
    t.value = float(t.value)    
    return t

def t_error(t):
	print "ERROR: "+str(t)+" unexpected"

import ply.lex as lex
lexer = lex.lex()

def tokenList(str):
	result = []
	lexer.input(str)
	tok = lexer.token()
	while (tok):
		result.append(tok)
		tok = lexer.token()
	return result

if __name__=="__main__":
	while (True):
		s = raw_input()
		print tokenList(s)	