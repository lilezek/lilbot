import Tokenizer
"""This file contains assorted general utility functions used by other
modules in the PyAIML package.

"""

def sentences(s):
    s = Tokenizer.tokenList(s)
    result = []
    for tok in s:
        result.append(tok)
        if (tok.type == "FIN"):
            yield result
            result = []
    if (len(result)):
        yield result

# Self test
if __name__ == "__main__":
    # sentences
    sents = sentences("First.  Second, still?  Third and Final!  Well, not really")
    for s in sents:
        print s
