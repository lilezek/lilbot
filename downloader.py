#!/usr/bin/env python
# -*- coding: utf-8 -*-

import imghdr
import os
import re
import urllib2
import uuid

class Downloader:
	_imgCheck = re.compile("(?i)Content-Type: image")
	_sizeCheck = re.compile("(?i)Content-Length: (\\d+)")
	
	def __init__(self, folder = "./downs/",maxSize = 1048576, blockSize = 4096):
		self._maximumSize = maxSize
		self._downloadFolder = folder
		self._blockSize = blockSize

	def downloadImage(self, url, filename=None):
		if (filename is None):
			filename = self._downloadFolder+str(uuid.uuid4())
		with open(filename,"w") as self._lastfile:
			conn = self._openImageURL(url)
			if (conn):
				self._download(conn)
		ext = imghdr.what(filename)
		if (ext is not None):
			os.rename(filename,filename+"."+ext)
			return filename+"."+ext
		else:
			os.remove(filename)
		return None

	def _download(self, connection):	
		size = 0
		for k in connection.info().headers:
			m = self._sizeCheck.search(k)
			if (m):
				size = int(m.group(1))
		if (size > self._maximumSize):
			connection.close()
			raise SizeLimitError(size)
		block = connection.read(self._blockSize)
		count = len(block)
		while (len(block) and self._getBlock(count,block)):
			block = connection.read(self._blockSize)
			count += len(block)
		connection.close()
		if (count > self._maximumSize):
			raise SizeLimitError(count)
		return True

	def _getBlock(self, count, block):
		if (count <= self._maximumSize):
			self._lastfile.write(block)
			return True
		return False

	def _openImageURL(self,url):
		try:
			conn = urllib2.urlopen(url)
		except urllib2.HTTPError as e:
			return None
		isImage = False
		size = 0
		for k in conn.info().headers:
			if (self._imgCheck.search(k)):
				isImage = True
		return isImage and conn

class SizeLimitError(StandardError):
	def __init__(self, size):
		self._size = size
		super(SizeLimitError, self).__init__("The size "+str(size)+" is bigger than limit")
	def getSize(self):
		return self._size
