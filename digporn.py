#!/usr/bin/python

#http://www.porndig.com/search/videos/<tags>
#http://www.porndig.com/videos/<id>

import urllib2
import getopt
import sys
import random
import string
from HTMLParser import HTMLParser
from cookielib import CookieJar
import re
import cProfile
from cache import Cache

# Regular expression for web scrapping:
idFromLink = re.compile("/videos/([0-9]*)/")

#firefox-like opener
cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [("User-Agent","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0"),
	("Accept","application/json, text/javascript, */*; q=0.01"),
	("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3"),
	("Cache-Control","no-cache"),
	("Content-Length","0"),
	("Referer","http://www.porndig.com/")]


class PornDigHTMLParserList(HTMLParser):
	_in_video = 0
	_video_ids = []
	_content_normal_attr = ("class","content normal")
	_target_self = ("target","_self")
	def handle_starttag(self, tag, attrs):
		if (self._in_video > 0):
			if (tag == "div"):
				self._in_video += 1
			elif (tag == "a" and self._target_self in attrs):
				for attr,val in attrs:
					if (attr == "href"):
						videoid = idFromLink.search(val)
						if (videoid):
							self._video_ids.append(videoid.group(1))
							break
		else:
			if (tag == "div" and self._content_normal_attr in attrs):
				self._in_video += 1
	def handle_endtag(self, tag):
		if (self._in_video > 0):
			if (tag == "div"):
				self._in_video -= 1
	def handle_data(self, data):
		pass
	def getVideoIDs(self):
		return self._video_ids

class PornDigHTMLParserDownload(HTMLParser):
	_in_tr = 0
	_in_quality = False
	_video_links = {}
	_last_video_link = ""
	_last_quality = ""
	_class_download_link = ("class","download_link")
	_id_download_quality = ("id","download_quality")
	def handle_starttag(self, tag, attrs):
		if (self._in_tr > 0):
			if (tag == "tr"):
				self._in_tr += 1
			elif (tag == "a"):
				for attr,val in attrs:
					if (attr == "href"):
						self._last_video_link = val
						break
			elif (tag == "span" and self._id_download_quality in attrs):
				self._in_quality = True
		else:
			if (tag == "tr" and self._class_download_link in attrs):
				self._in_tr += 1
	def handle_endtag(self, tag):
		if (self._in_tr > 0):
			if (tag == "tr"):
				self._in_tr -= 1
				self._video_links[self._last_quality] = self._last_video_link
			if (tag == "span"):
				self._in_quality = False
	def handle_data(self, data):
		if (self._in_quality):
			self._last_quality = data
	def getVideoLinks(self):
		return self._video_links

def porndigGetList(key):
	response = opener.open("http://www.porndig.com/search/videos/"+urllib2.quote(key))
	data = response.read()
	response.close()
	parser = PornDigHTMLParserList()
	parser.feed(data)
	vids = parser.getVideoIDs()
	return vids

cGetList = Cache("./cache",porndigGetList)

def porndigGetQualities(videoID):
	response = opener.open("http://www.porndig.com/videos/"+str(videoID))
	data = response.read()
	response.close()
	parser = PornDigHTMLParserDownload()
	parser.feed(data)
	vids = parser.getVideoLinks()
	return vids

cGetQualities = Cache("./cache",porndigGetQualities)

def porndigID(key, number):
	vids = cGetList(key)
	if (len(vids) > 0):
		return vids[number%len(vids)]
	return None

def porndigScrapeAttributes(url):
	response = opener.open(url)
	return response.geturl()

def porndigScrape(videoID,quality=None):
	vids = cGetQualities(videoID)
	if (quality is not None and quality in vids.keys()):
		return porndigScrapeAttributes(vids[quality])
	if ("1080p" in vids.keys()):
		return porndigScrapeAttributes(vids["1080p"])
	if ("720p" in vids.keys()):
		return porndigScrapeAttributes(vids["720p"])
	if ("540p" in vids.keys()):
		return porndigScrapeAttributes(vids["540p"])
	if ("360p" in vids.keys()):
		return porndigScrapeAttributes(vids["360p"])
	if ("270p" in vids.keys()):
		return porndigScrapeAttributes(vids["270p"])


def usage(ex):
	print "Usage: %s [-q quality] [-i id] [-r seed] [-k key] [-h|--help] " % sys.argv[0]
	sys.exit(ex)

def main():
	keys = "full hd"
	srand = None
	vid = None
	qual = None
	try:
		opts, args = getopt.getopt(sys.argv[1:], "q:i:r:k:h", [])
	except getopt.GetoptError:
		usage(2)
	if (("-h","") in opts):
		usage(0)
	for arg,val in opts:
		if arg == "-r":
			try:
				srand = int(val)
			except ValueError:
				usage(2)
		if arg == "-k":
			keys = val
		if arg == "-i":
			vid = val
		if arg == "-q":
			qual = val;
	if (srand is not None):
		random.seed(srand)
	if (vid is None):
		n = random.randint(1,2000)
		vid = porndigID(keys,n)
	print vid and porndigScrape(vid,qual)

if __name__ == "__main__":
	main()
	