import marshal
import md5
import os

_CachePool = set()

def flushAllCaches():
	for c in _CachePool:
		c.flush()

class Cache:
	_directory = ""
	_function = None
	_cached = {}
	_cached_from_disk = set()
	_misses = 0
	_success = 0
	def __init__(self,directory,function):
		self._directory = directory
		if (not hasattr(function,"__call__")):
			raise ValueError("#3 argument must be callable")
		self._function = function
		if not os.path.exists(directory):
			os.makedirs(directory)
		_CachePool.add(self)

	def __call__(self, *args):
		h = self.argshash(*args)
		# It is cached:
		value = self.cached(h,*args)
		if (value):
			return value
		# It is not cached:
		value = self._function(*args)
		self.addcache(h,value, *args)
		return value

	def argshash(self, *args):
		return md5.new(marshal.dumps(*args)).hexdigest()

	def addcache(self, key, value, *args):
		self._cached[key] = (value, args)

	def checksuccess(self, key, *args):
		if (key in self._cached.keys()):
			return True
		if (self.load(key,*args)):
			if (key in self._cached.keys()):
				return True
		return False

	def load(self, key, *args):
		f = self._directory+"/"+key+".csh"
		if (os.path.isfile(f)):
			f = open(f,"rb")
			pair = marshal.load(f)
			f.close()
		else:
			return False
		if (pair[1] == args):
			self._cached[key] = pair
			self._cached_from_disk.add(key)
			return True
		return False

	def cached(self, key, *args):
		if (self.checksuccess(key,*args)):
			self._success += 1
			return self._cached[key][0]
		self._misses += 1
		return None

	def save(self, key,value):
		f = self._directory+"/"+key+".csh"
		f = open(f,"w")
		marshal.dump(value,f)
		f.close()

	def flush(self):
		for k,v in self._cached.iteritems():
			if (k not in self._cached_from_disk):
				self.save(k,v)

if __name__ == "__main__":
	import time
	import sys
	def a(x):
		time.sleep(3)
		return x*x

	ac = Cache("./cache",a)
	print ac(int(sys.argv[1]))
