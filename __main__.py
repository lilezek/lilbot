from chatbot import chatbot
import urllib, urllib2
import json
import traceback
from telegrambot import *

token = open("lilbot.key", "r").read().replace('\n','');

def telegramBotGet(api):
	return urllib2.urlopen("https://api.telegram.org/bot"+token+"/"+api).read()

def telegramBotPost(api, attrs):
	url = "https://api.telegram.org/bot"+token+"/"+api
	data = urllib.urlencode(attrs)
	req = urllib2.Request(url, data)
	response = urllib2.urlopen(req)
	the_page = response.read()
	return the_page

def sendTelegramMessage(chat, message, replyTo = ""):
	attrs = {"chat_id" : chat, "text" : message}
	if (replyTo != ""):
		attrs["reply_to_message_id"] = replyTo
	return telegramBotPost("sendMessage",attrs)

global lastMessageId
def getLastMessageId():
	return str(lastMessageId)


def getTelegramMessages(offset = 0):
	if (offset > 0):
		result = json.loads(telegramBotPost("getUpdates", {'offset' : offset+1, "timeout": 15}))
	else:
		result = json.loads(telegramBotGet("getUpdates"))
	if (result["ok"]):
		return result["result"]
	return None

global bot
def main():
	global bot 
	global lastMessageId
	lastMessageId = 0
	with open("lastid.txt","r") as f:
		lastMessageId = int(f.read().replace('\n',''))
	bot = chatbot()
	bot.loadAllSessions()
	while True:
		print "running"
		messages = getTelegramMessages(lastMessageId)
		for m in messages:
			msg = TelegramMessage.FromDict(m['message'])
			try:
				uid = m['update_id']
				lastMessageId = max(lastMessageId, uid) 
				sendTelegramMessage(msg.chat.id, bot.respond(msg.ufrom.id, msg.text), msg.message_id)
			except KeyError as e:
				print "KeyError", e
				print m
	save()
	

def fatal():
	save()

def save():
	global bot
	bot.storeAllSessions()
	with open("lastid.txt","w") as f:
		f.write(getLastMessageId())

if __name__ == "__main__":
	try:
		main()
	except:
		print(traceback.format_exc())
		fatal()
	
