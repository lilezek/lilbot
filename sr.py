#!/usr/bin/python

import sys      #for cmd line argv
import time     #for delay
import gi
import urllib
from gi.repository import Gst

Gst.init(None)

class TextToSpeech:
	def __init__(self):
		pass

	def speak(self, str):
		self._player = Gst.ElementFactory.make("playbin", None)
		self._player.set_property('uri', 'http://translate.google.com/translate_tts?tl=es&q='+urllib.quote_plus(str))
		self._player.set_state(Gst.State.PLAYING)