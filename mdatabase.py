from lilAIML import module
import sqlite3
import Queue

# Archivo a almacenar el conocimiento
cortexFile = "./cortex.db"

# Variables a leer de los aiml:
# Valor a incluir en el TO-DO
actionStr   = "learn_action" # (store, loadlist, check, ponder)
objectStr   = "learn_object"
propStr     = "learn_prop"
ponderStr   = "learn_ponder"

class moduleLearn(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer):
        return True
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "learn"

    def createDatabase(self):
        pass

    def getKnowledgeToken(self, object):
        pass

    def knowledgeEvent(self, obj, prop, src, ponder=1):
        pass


"""
Un token de conocimiento es una cuaterna (objeto,propiedad,valorpositivo,valornegativo)
donde esto significa que el objeto tiene la propiedad con cierta seguridad. 

Seguridad es la probabilidad experimental que se sabe que ha sucedido la propiedad en el objeto.

Por ejemplo, suponiendo que Bob tiene fiabilidad del 100%.

El sol es azul
El sol es rojo
El sol es amarillo
El sol no es azul
El sol no es transparente

daría las cuaternas

(sol,amarillo,1,0)
(sol,rojo,1,0)
(sol,azul,1,1)
(sol,transparente,0,1)
"""

"""
Una fuente es una entidad que modifica los conocimientos, y tiene un valor de fiabilidad.
Si una fuente contradice un conocimiento, se suma uno en negativo. Por otro lado,
si una fuente añade un conocimiento nuevo o modifica positivamente un conocimiento, se suma uno en positivo.

Por ejemplo:

Antes:

(sol,amarillo,1,0)
(sol,transparente,0,1)
(Bob,0,0) <-- Fiabilidad del 0%

El sol es rojo. <-- Fiabilidad del 100%
El sol es amarillo. <-- Fiabilidad del 100%
El sol es transparente. <-- Fiabilidad del 50%

Después:

(sol,amarillo,2,0)
(sol,rojo,1)
(sol,transparente,1,1)
(Bob,2,1)
"""

"""
Se asume que algo es verdad, si cumple la siguiente fórmula

p := intentos positivos
n := intentos negativos
t := población completa

p/n > log(t)

Por ejemplo, si hay 1024 positivos y 1024 negativos, la fórmula

1 > 10 no se cumpliría, pero 500 positivos y 50 negativos, la fórmula

500/50 = 10 > log(550)

sí se cumpliría.

Por otro lado se asume que algo es mentira, si cumple la misma fórmula cambiando
la fracción p/n por n/p.

En otro caso, la sentencia es no concluyente (none). 

Usando esta lógica, los operadores básicos:

true ^ none = none
false ^ none = false
true v none = true
false v none = none
¬none = none
"""
