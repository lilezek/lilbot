from lilAIML import module;
import Queue
import urllib


# Variables a leer de los aiml:
# Valor a incluir en el TO-DO
latexValue = "latex_value" 

class moduleLatex(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (latexValue in session.keys()):
            val = session[latexValue]
            kern.setPredicate(latexValue,None,peer)
            if (val != ""):
                val = "http://latex.codecogs.com/png.latex?"+urllib.quote(val)
                self.q.put(val);
                return True
        print "WARNING: no latexValue given"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "latex"