#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lilAIML import module;
import Queue;
import wikipedia;

#Configuración
truncateText = 200 # Número máximo de letras que puede emitir el módulo de la wikipedia.

#Variables a pasar:
wikiSearchStr = "wiki_search_terms"

class moduleWiki(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()
        wikipedia.set_lang("es")

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (wikiSearchStr in session.keys()):
            val = session[wikiSearchStr]
            kern.setPredicate(wikiSearchStr,None,peer)
            if (val != ""):
                try:
                    summary = wikipedia.summary(val)
                    if (len(summary) > truncateText):
                        self.q.put(summary[:truncateText-3]+"...")
                    else:
                        self.q.put(summary)
                    return True
                except wikipedia.exceptions.DisambiguationError as e:
                    msg = "Existen varios resultados sobre " + val+ "\n"
                    for option in wikipedia.search(val):
                        msg += option + "\n"
                    self.q.put(msg) 
                    return True
                except wikipedia.exceptions.PageError as e:
                    self.q.put("No se nada sobre "+val)
                    return True
        print "Error: wiki search without query terms"
        return False


    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()


    def getToken(self):
        return "wiki"
