#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import re
from lilAIML import module;
import Queue
import random

# Valores a pasar del aiml:
# Términos de la búsqueda
googleImageSearchStr = "google_image_terms" # Debe ser un string
googleImageIndexStr  = "google_image_index" # Debe ser un entero no negativo

def gimage(terms, index): 
    # q : terminos
    # hl : idioma
    query = urllib.urlencode ( { 'q' : terms.encode("utf-8") , 'hl' : "es", 'start' : str(index), "safe" : "off"} )
    response = urllib.urlopen ( 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0&' + query ).read()
    diccionario = json.loads(response)
    
    results = diccionario [ 'responseData' ] [ 'results' ]
    if (len(results) > 0):
        return results[0]['unescapedUrl']
    return None

class moduleGoogleImage(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        if (googleImageSearchStr in session.keys()):
            val = session[googleImageSearchStr]
            val2 = session[googleImageIndexStr]
            kern.setPredicate(googleImageSearchStr,None,peer)
            kern.setPredicate(googleImageIndexStr,None,peer)
            if (val != ""):
                try:
                    val2 = int(val2)
                except ValueError:
                    val2 = random.randint(0,20)
                r = gimage(val,val2)
                if (r):
                    self.q.put(r)
                else:
                    self.q.put(u"No he encontrado ninguna imagen sobre ese tema")
                return True
        print "Error: google image search without query terms"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    
    def getToken(self):
        return "googleimage"
