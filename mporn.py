#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lilAIML import module;
import Queue;
import digporn;
import random;
import string;

#Variables a pasar:
pornTagSearch = "porn_search_terms" # varias palabras separadas por espacio
pornTagID     = "porn_tag_id"       # el id del vídeo a ver, si se conoce


class modulePorn(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer):
        session = kern.getSessionData(peer)
        tags = None
        vid = None
        if (pornTagSearch in session.keys() and session[pornTagSearch] is not None):
            tags = session[pornTagSearch]
            tags = string.join(tags.split(),"+")
            kern.setPredicate(pornTagSearch,None,peer)
        if (pornTagID in session.keys() and session[pornTagID] is not None):
            vid = session[pornTagID]
            kern.setPredicate(pornTagID,None,peer)
        tags = tags or "all"
        vid = vid or digporn.porndigID(tags,random.randint(1,2000))
        if (vid is None):
            self.q.put("No conozco la categoría "+tags.replace("+"," "))
            return True
        self.q.put(digporn.porndigScrape(vid,"540p"))
        return True


    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()


    def getToken(self):
        return "porn"
