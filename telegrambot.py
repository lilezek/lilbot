class TelegramChat:
	user = None
	group = None

	# Inherited data:
	id = 0

	def __eq__(self, other):
		return self.user == other.user and other.group == self.group

	@staticmethod 
	def FromDict(d):
		result = TelegramChat()
		if ('title' in d.keys()):
			result.group = TelegramGroup.FromDict(d)
			result.id = result.group.id
		else:
			result.user = TelegramUser.FromDict(d)
			result.id = result.user.id
		return result

class TelegramGroup:
	id = 0
	title = ""
	
	def __init__(self, id=-1):
		self.id = id

	def __eq__(self, other):
		return self.id == other.id

	@staticmethod 
	def FromDict(d):
		result = TelegramGroup(d['id'])
		result.title = d['title']
		return result



class TelegramUser:
	id	= 0 
	first_name	= ""	
	#last_name	= ""	Optional. User's or bot's last name
	#username	= ""	Optional. User's or bot's username

	def __init__(self, id=-1):
		self.id = id

	def __eq__(self, other):
		return self.id == other.id

	@staticmethod 
	def FromDict(d):
		result = TelegramUser(d['id'])
		result.first_name = d['first_name']
		return result


class TelegramMessage:
	message_id	= 0	
	ufrom	= TelegramUser(None)
	date	= 0
	chat	= TelegramUser(None)
	#forward_from	= TelegramUser(None)	Optional. For forwarded messages, sender of the original message
	#forward_date	= 0	Optional. For forwarded messages, date the original message was sent in Unix time
	#reply_to_message	Message	Optional. For replies, the original message. Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
	text	= ""
	#audio	Audio	Optional. Message is an audio file, information about the file
	#document	Document	Optional. Message is a general file, information about the file
	#photo	Array of PhotoSize	Optional. Message is a photo, available sizes of the photo
	#sticker	Sticker	Optional. Message is a sticker, information about the sticker
	#video	Video	Optional. Message is a video, information about the video
	#contact	Contact	Optional. Message is a shared contact, information about the contact
	#location	Location	Optional. Message is a shared location, information about the location
	#new_chat_participant	= TelegramUser(None)	Optional. A new member was added to the group, information about them (this member may be bot itself)
	#left_chat_participant	= TelegramUser(None)	Optional. A member was removed from the group, information about them (this member may be bot itself)
	#new_chat_title	= ""	Optional. A group title was changed to this value
	#new_chat_photo	Array of PhotoSize	Optional. A group photo was change to this value
	#delete_chat_photo	True	Optional. Informs that the group photo was deleted
	#group_chat_created	True	Optional. Informs that the group has been created

	def __init__(self, mid = -1):
		self.message_id = mid

	def __eq__(self, other):
		return self.message_id == other.message_id

	@staticmethod 
	def FromDict(d):
		result = TelegramMessage(d['message_id'])
		result.ufrom = TelegramUser.FromDict(d['from'])
		result.date = d['date']
		result.chat = TelegramChat.FromDict(d['chat'])

		# Optionals
		if ('text' in d.keys()):
			result.text = d['text'][1:]

		return result